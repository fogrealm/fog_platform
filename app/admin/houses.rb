ActiveAdmin.register House do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name, :address, :landmark, :description,
                location_attributes: [ :id, :latitude, :longitude ],
                house_sections_attributes: [ :id, :name, :_destroy ]

  actions :all, :except => [ :new, :destroy]

  index do
    selectable_column
    id_column
    column :name
    column :address
    column :landmark
    column :location do |house|
      "#{house.try(:location).try(:latitude)} , #{house.try(:location).try(:longitude)} "
    end
  end

  show do |house|
    attributes_table do
      row :name
      row :address
      row :landmark
      row :description
    end

    panel 'Location' do
      attributes_table_for house.location do
        row :longitude
        row :latitude
      end
    end

    attributes_table do
      row :house_sections do |house|
        bullet_list(house.house_sections.map(&:name), nil)
      end
    end
    
  end

  form do |f|
    f.inputs 'House Details' do
      f.semantic_errors *f.object.errors.keys

      f.input :name
      f.input :address
      f.input :landmark
      f.input :description

      f.inputs "Location Details", for: [ :location, f.object.location || f.object.build_location ] do |location_form|
        location_form.input :latitude
        location_form.input :longitude
      end

      
      f.inputs 'House Sections' do

        f.has_many :house_sections do |house_section_form|
          house_section_form.input :_destroy, :as => :boolean, :label => "Destroy?" unless house_section_form.object.new_record? 
          house_section_form.input :name
        end
      end

      f.actions
    end
  end
end