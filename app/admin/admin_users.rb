ActiveAdmin.register AdminUser do
  permit_params :email, :name, :password, :password_confirmation, :phone_number

  index do
    selectable_column
    id_column
    column :email
    column :name
    column :phone_number
  
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  show do |admin_user|
    attributes_table do
      row :email
      row :name
      row :phone_number
      row :current_sign_in_at
      row :sign_in_count
      row :created_at 
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :email
      f.input :name
      f.input :password
      f.input :password_confirmation
      f.input :phone_number
    end
    
    f.actions
  end

end
