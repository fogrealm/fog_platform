ActiveAdmin.register ServiceInfra do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name, :priority

  actions :all

  index do
    selectable_column
    id_column
    column :name
    column :house
    
    column :no_of_end_devices do |service_infra|
      " #{service_infra.end_devices.count} "
    end
    
    actions
  end

  show do |service_infra|
    attributes_table do
      row :name
      row :house
    end

    panel " End Device(s) Information " do
      attributes_table do
        row "No of End Devices" do |service_infra|
          " #{service_infra.end_devices.count} "
        end

        row "End Devices with Information" do |service_infra|
          bullet_list(service_infra.end_devices.map{|end_device| " #{end_device.name} ( #{end_device.try(:end_device_info).try(:name) } ), #{end_device.try(:house_section).try(:name)} " }, nil)
        end
      end
    end
  end

  form do |f|
    f.inputs 'House Section Details' do
      f.semantic_errors *f.object.errors.keys

      f.input :name
      f.input :priority, as: :select, collection: ServiceInfra::MIN_PRIORITY .. ServiceInfra::MAX_PRIORITY, selected: ServiceInfra::MIN_PRIORITY
      f.input :house, as: :hidden, input_html: { :value => House.first }

      f.actions
    end
  end


end
