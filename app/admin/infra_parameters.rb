ActiveAdmin.register InfraParameter do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name, :unit, :min_value, :max_value, :fractional_accuracy

  actions :all

  index do
    selectable_column
    id_column
    column :name
    column :unit

    #column "Value Range Permitter" do |infra_parameter|
    #  " #{infra_parameter.min_value} - #{infra_parameter.max_value} "
    #end

    #column :fractional_accuracy

    actions
  end

  show do |infra_parameter|
    attributes_table do
      row :name
      row :unit
      #row "Value Range Permitter" do |infra_parameter|
      #  " #{infra_parameter.min_value} - #{infra_parameter.max_value} "
      #end

      #row :fractional_accuracy
    end
  end

  form do |f|
    f.inputs 'House Details' do
      f.semantic_errors *f.object.errors.keys

      f.input :name
      f.input :unit
      #f.input :min_value
      #f.input :max_value
      #f.input :fractional_accuracy
      
      f.actions
    end
  end

end
