class InfraReadingsController < InheritedResources::Base
	respond_to :json

	private

  def permitted_params
    params.permit(infra_reading: [:id, :infra_parameter_id, :end_device_id, :value])
  end
end
