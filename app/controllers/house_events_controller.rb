class HouseEventsController < InheritedResources::Base
  respond_to :json

  def fire_alarm_controller
    
    service_task_entity_map = params[:service_task_entity_map]

    Rails.logger.info "\n Reaching here - fire alarm controller"
    
    valid_request = true

    valid_request = false if service_task_entity_map.nil? || service_task_entity_map[:house_section].nil? || service_task_entity_map[:house_section][:id].nil?
    
        Rails.logger.info "\n valid request -> #{valid_request} -> #{service_task_entity_map}"

    valid_request = false if service_task_entity_map.nil? || service_task_entity_map[:infra_reading].nil? || service_task_entity_map[:infra_reading][:infra_parameter].nil? || service_task_entity_map[:infra_reading][:infra_parameter][:id].nil?

    Rails.logger.info "\n valid request -> #{valid_request} -> #{service_task_entity_map}"

    if valid_request == true
      infra_parameter_id = service_task_entity_map[:infra_reading][:infra_parameter][:id]
      house_section_id = service_task_entity_map[:house_section][:id]
      measurement = service_task_entity_map[:infra_reading][:value]

      $redis.set("controller_cached_house_section_id_#{house_section_id}_infra_parameter_id_#{infra_parameter_id}_reading", measurement)
      #result = $redis.get("controller_house_section_id_#{house_section_id}_infra_parameter_id_#{infra_parameter_id}_reading", measurement)
     
      Rails.logger.info "\n\n map received -> #{service_task_entity_map} - {result}"
      
      HouseEvent.monitor_fire_occurence if HouseEvent.fire_detection_scheduler.nil?

      render json: {
        status: 200,
        message: "Information is cached in controller"
      } and return
    else 
      
      render json: {
        status: 422,
        error: "Message from Rails Controller Instance Invalid Request from "
      } and return
    end
  end

  def smart_meter_controller
    service_task_entity_map = params[:service_task_entity_map]

    Rails.logger.info "\n Reaching here - smart meter controller" 
    
    valid_request = true

    valid_request = false if service_task_entity_map.nil? || service_task_entity_map[:house_section].nil? || service_task_entity_map[:house_section][:id].nil?
    
    Rails.logger.info "\n valid request -> #{valid_request} -> #{service_task_entity_map}"

    valid_request = false if service_task_entity_map.nil? || service_task_entity_map[:infra_reading].nil? || service_task_entity_map[:infra_reading][:infra_parameter].nil? || service_task_entity_map[:infra_reading][:infra_parameter][:id].nil?

    Rails.logger.info "\n valid request -> #{valid_request} -> #{service_task_entity_map}"


    if valid_request == true
      infra_parameter_id = service_task_entity_map[:infra_reading][:infra_parameter][:id]
      house_section_id = service_task_entity_map[:house_section][:id]
      measurement = service_task_entity_map[:infra_reading][:value]

      $redis.set("controller_cached_house_section_id_#{house_section_id}_infra_parameter_id_#{infra_parameter_id}_reading", measurement)
      #result = $redis.get("controller_house_section_id_#{house_section_id}_infra_parameter_id_#{infra_parameter_id}_reading", measurement)
     
      Rails.logger.info "\n\n map received -> #{service_task_entity_map} - {result}"
      
      HouseEvent.monitor_smart_meter if HouseEvent.smart_meter_scheduler.nil?

      render json: {
        status: 200,
        message: "Information is cached in controller"
      } and return
    else 
      
      render json: {
        status: 422,
        error: "Message from Rails Controller Instance Invalid Request from "
      } and return
    end


  end

  def interconnecting_fog_controller

    house_id = params[:house_id]
    infra_parameter_id = params[:infra_parameter_id]
    measurement = params[:measurement]

    if house_id.present? && infra_parameter_id.present? && measurement.present?
      
      house_ids_json = $redis.get("cached_house_ids_json")

      if house_ids_json.nil? || house_ids_json == ""
        new_house_ids_json = [ house_id ].to_json
        $redis.set("cached_house_ids_json", new_house_ids_json)
      else
        house_ids_array = JSON.parse(house_ids_json)
        house_ids_array << house_id
        new_house_ids_json = house_ids_array.to_json
        $redis.set("cached_house_ids_json", new_house_ids_json)
      end

      $redis.set("cached_icfn_entry_#{house_id}_#{infra_parameter_id}", measurement)
    else
      render json: {
        status: 422,
        error: "Message from Interconnecting Fog Controller Instance Invalid Request from Fog"
      } and return
    end


  end

  def test_events_controller
    render json: {
      status: 200,
      message: "Success"
    } and return
  end


  private

  def permitted_params
    params.permit(house_event: [:id, :name, :event_level, :house_event_type, :end_device_id])
  end

end
=begin
  
  service_task_entity_map = {
      :gateway_timestamp => gateway_timestamp,
      :service_priority => priority,
      service_infra: {
        id: service_infra.id,
        name: service_infra.name,
        priority: service_infra.priority
      },
      :infra_reading => {
        :infra_parameter => {
          :id => infra_parameter_id,
          :name => infra_parameter_name,
          :unit => infra_parameter_unit
        },
        :value => measurement.to_f,
        :end_device => {
          :id => end_device_id
        }
      },
      :retry_count => 0,
      :max_retry_count => ServiceTask::END_DEVICE_DATA_INFORM_MAX_RETRY_COUNT,
      :enqueued_timestamp => Time.now.to_i,
      :service_task_type => ServiceTask::SERVICE_TASK_TYPE_END_DEVICE_DATA_INFORMATION
    }


=end