class FogMain

  attr_accessor :fog_simulator, :service_task_manager

  def initialize(args)
    self.fog_simulator = nil
    self.service_task_manager = nil
  end

  def self.initiate_layer1_services

    fire_alarm_device_gateway = DeviceGateway.find(1)
    smart_meter_device_gateway = DeviceGateway.find(2)

    service_task_manager = ServiceTaskManager.new

    simulator = Simulator.new
    simulator.fetch_simulated_data(fire_alarm_device_gateway)
    simulator.fetch_simulated_data(smart_meter_device_gateway)
    #simulator.fetch_simulated_data(smart_meter_device_gateway)

    Rails.logger.info "\n After 1"
    
    service_task_manager.generate_service_tasks
    
    Rails.logger.info "\n After 2"

    service_task_manager.task_distribution_algorithm

    Rails.logger.info "\n After 3"
  end

  
  def self.enable_interconnection_fog_nodes
    controller_connections = {}

    port_available_map = {}
    ServiceTaskManager::RAILS_INSTANCES_PORT_RANGE.to_a.each do |rails_instance_port_no|
      port_available_map[rails_instance_port_no] = true
    end

    ServiceTaskManager::RAILS_INSTANCES_PORT_RANGE.to_a.each do |rails_instance_port_no|
      conn = Faraday.new( :url => "http://0.0.0.0:#{rails_instance_port_no}" ) do |faraday|
        faraday.request  :url_encoded             # form-encode POST params
        faraday.response :logger                  # log requests to STDOUT
        faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
        faraday.headers['Content-Type'] = 'application/json'
      end

      controller_connections[rails_instance_port_no] = conn
    end

    icfn_scheduler = Rufus::Scheduler.new
    icfn_scheduler.every '10s' do
      ServiceTaskManager::RAILS_INSTANCES_PORT_RANGE.to_a.each do |rails_instance_port_no|
        test_connection = controller_connections[rails_instance_port_no]
        response = test_connection.get "/house_events/test_events_controller.json" rescue $icfn_logger.info "Fog Node with #{rails_instance_port_no} failed"
      end
    end

    #HouseEvent.enable_interconnecting_forward_scheduler if HouseEvent.icfn_forwarding_scheduler.nil?

  end
  
end