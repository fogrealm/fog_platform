module Conditions::EndDevice

  def is_device_class_device?
    # => Checks whether current end device class is a class type 'device' ...

    self.end_device_class == EndDevice::END_DEVICE_CLASS_DEVICE
  end


  def is_device_class_gateway?
    # => Checks whether current end device class is a class type 'gateway' ...

    self.end_device_class == EndDevice::END_DEVICE_CLASS_GATEWAY
  end

end