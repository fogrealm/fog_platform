class Profile < ApplicationRecord
  include Formats::Profile
  include Attributes::Profile

  # => Relations ...
  has_one :user

  belongs_to :image, :class_name => Attachment, :dependent => :destroy

  delegate :attachment_url, to: :image

  
  # => Validations ...
  validates_length_of :first_name, minimum: 4
  #validates :dob, presence: true

  
  # => Associations ...
  accepts_nested_attributes_for :image

  
  # => Scopes ...
  default_scope { 
    includes(:user) 
  }

  
  # => Callbacks ...
  before_validation :map_dob_with_dob_parameters

  
  # => Accessors ...
  attr_accessor :dob_day, :dob_month, :dob_year

  private

  def map_dob_with_dob_parameters
    # => We map the date of birth parameters we obtain from json to update the dob field in this model ...
    # => PS. This is only meant for json requests ...

    # => In case of update ...
    # => update the local attributes with the one we have saved on DB ...
    if self.dob.present?
      self.dob_day = self.dob.day
      self.dob_month = self.dob.month
      self.dob_year = self.dob.year
    end

    if ( self.dob_day && self.dob_month && self.dob_year ).present?
      self.dob = Date.new(self.dob_year, self.dob_month, self.dob_day)
    end

    puts "\n\n date of birth -> #{self.dob}"
  end

end
