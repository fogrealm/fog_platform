module Formats::Location
  
  def as_json( opt = {} )
    {
      id: self.id,
      latitude: self.latitude, 
      longitude: self.longitude
    }
  end
  
end