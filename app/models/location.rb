class Location < ApplicationRecord
  include Formats::Location

  # => Relations ...
  belongs_to :locatable, polymorphic: true


  # => Validations ...
  validates :latitude, :longitude, presence: true

end
