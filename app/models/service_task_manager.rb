class ServiceTaskManager

  # => Constants ...
  MAX_TIME_SLOTS = ServiceInfra::MAX_PRIORITY * ServiceInfra::MAX_PRIORITY
  SCHEDULE_INTERVAL = '4s'
  END_DEVICE_DATA_INFORM_MAX_RETRY_COUNT = 5

  RAILS_INSTANCES_PORT_RANGE = 4000 .. 4002


  # => Accessors ...
  attr_accessor :service_task_scheduler, :task_algorithm_scheduler, 
                :all_service_infras, :all_infra_parameter_mappings, 
                :all_end_device_id_house_section_mappings,
                :service_task_pqueue,:controller_connections,
                :admin_users_array_map, :port_available_map


  def initialize
    # => Initialize the  basic components or variables of service task manager ...

    self.all_service_infras = ServiceInfra.all
    self.service_task_pqueue = MinPriorityQueue.new

    self.service_task_scheduler = Rufus::Scheduler.new
    self.task_algorithm_scheduler = Rufus::Scheduler.new

    self.all_infra_parameter_mappings = InfraParameter.all.map{ |infra_parameter| [[ infra_parameter.name, infra_parameter.unit ], infra_parameter.id] }.to_h
    self.all_end_device_id_house_section_mappings = EndDevice.all.map{|end_device| [ end_device.id, [ end_device.house_section.id, end_device.house_section.name]] }.to_h

    self.admin_users_array_map = AdminUser.all.as_json

    self.controller_connections = {}

    self.port_available_map = {}
    RAILS_INSTANCES_PORT_RANGE.to_a.each do |rails_instance_port_no|
      self.port_available_map[rails_instance_port_no] = true
    end

    RAILS_INSTANCES_PORT_RANGE.to_a.each do |rails_instance_port_no|
      conn = Faraday.new( :url => "http://0.0.0.0:#{rails_instance_port_no}" ) do |faraday|
        faraday.request  :url_encoded             # form-encode POST params
        faraday.response :logger                  # log requests to STDOUT
        faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
        faraday.headers['Content-Type'] = 'application/json'
      end

      self.controller_connections[rails_instance_port_no] = conn
    end    
  end


  


  def generate_service_tasks
    # => function to generate service tasks and cache them in server ...

    time_slot_index = 1

    # => Wait till cache has values 

    while $redis.keys("cached_end_device_*_reading").empty? do
    end


    self.service_task_scheduler.every '2s' do


      #MAX_TIME_SLOTS.times.each do |time_slot_index|

      self.all_service_infras.each do |service_infra|
        end_devices = service_infra.end_devices

        end_devices.each do |end_device|

          infra_parameters = end_device.infra_parameters
          end_device_id = end_device.id

          infra_parameters.each do |infra_parameter|
          
            infra_parameter_name = infra_parameter.name
            infra_parameter_unit = infra_parameter.unit

            cached_data_values = $redis.get("cached_end_device_#{end_device_id}_infra_parameter_#{infra_parameter_name}_unit_#{infra_parameter_unit}_reading")
            next if cached_data_values.nil?

            #service_task = ServiceTask.new
            service_task_entity_map = generate_service_task_entity_map(cached_data_values, service_infra)
            #service_task.service_task_entity_map = service_task_entity_map

            next if service_task_entity_map.nil?

            enqueued_timestamp = service_task_entity_map[:enqueued_timestamp]

            if service_infra.priority % time_slot_index == 0
              self.service_task_pqueue.push service_task_entity_map, enqueued_timestamp
              puts "\n\n enqueued #{service_task_entity_map} - timestamp #{enqueued_timestamp}"
            end

          end
        end
      end

      time_slot_index = ( time_slot_index % MAX_TIME_SLOTS ) + 1

    end
  end


  def task_distribution_algorithm
    # => Performing the service task distribution ...

    self.task_algorithm_scheduler.every '2s' do

      while self.service_task_pqueue.size > 0

        service_task_entity_map = service_task_pqueue.pop

        #rails_instance_port_no = nil

        rails_instance_port_no = nil

        while rails_instance_port_no.nil?
          rails_instance_port_no = retrieve_available_port
        end

        self.port_available_map[rails_instance_port_no] = false

        FogNetworkManager.send_request_to_rails_controller(service_task_entity_map, rails_instance_port_no, controller_connections)
        
        self.port_available_map[rails_instance_port_no] = true


      end
    end
  end


  def retrieve_available_port
    # => We obtain the port which is not in Delayed Job or not currently in use ..

    self.port_available_map.map{| port_no, port_availability| port_no if port_availability == true }.compact.first
  end


  def generate_service_task_entity_map( cached_data_values , service_infra )
    return nil if cached_data_values.nil? || cached_data_values == ""

    cache_values_array = cached_data_values.split(",")

    return nil if cache_values_array.count != 5

    end_device_id = cache_values_array[0].to_i
    infra_parameter_name = cache_values_array[1].to_s
    infra_parameter_unit = cache_values_array[2].to_s
    measurement = cache_values_array[3].to_f
    gateway_timestamp = cache_values_array[4].to_i
    priority = service_infra.priority

    infra_parameter_id = self.all_infra_parameter_mappings[[infra_parameter_name, infra_parameter_unit]]
    house_section_id, house_section_name = self.all_end_device_id_house_section_mappings[end_device_id]

    service_task_entity_map = {
      :gateway_timestamp => gateway_timestamp,
      :service_priority => priority,
      :service_infra => {
        id: service_infra.id,
        name: service_infra.name,
        priority: service_infra.priority
      },
      :house_section => {
        id: house_section_id,
        name: house_section_name
      },
      :infra_reading => {
        :infra_parameter => {
          :id => infra_parameter_id,
          :name => infra_parameter_name,
          :unit => infra_parameter_unit
        },
        :value => measurement.to_f,
        :end_device => {
          :id => end_device_id
        }
      },
      :admin_users => admin_users_array_map,
      :retry_count => 0,
      :max_retry_count => ServiceTask::END_DEVICE_DATA_INFORM_MAX_RETRY_COUNT,
      :enqueued_timestamp => Time.now.to_i,
      :service_task_type => ServiceTask::SERVICE_TASK_TYPE_END_DEVICE_DATA_INFORMATION
    }

    service_task_entity_map
    # ServiceTask.save_in_db!(new_service_task.service_task_entity_map)
  end

end