class InfraParameter < ApplicationRecord
  # include Formats::InfraParameter

  # => Relations ...
  has_many :end_device_info_infra_parameters, dependent: :destroy
  has_many :end_device_infos, :through => :end_device_info_infra_parameters


  # => Validations ...
  validates :name, presence: true
  validates :unit, presence: true

  validates_uniqueness_of :name, scope: :unit
  #validates :min_value, presence: true
  #validates :max_value, presence: true
  #validates :fractional_accuracy, presence: true, inclusion: { :in => 0..10 }

end
