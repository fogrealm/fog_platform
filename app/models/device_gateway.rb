class DeviceGateway < ApplicationRecord
  # include Formats::DeviceGateway
  include Attributes::DeviceGateway

  # => Constants ...
  # => Below you are viewing are sample data from gateway ...

  # => Relations ...
  has_many :end_devices


  # => Validations ...
  validates :name, presence: true
  validates :base_url, presence: true
  
  # => Callbacks ...
  after_save :generate_device_gateway_url#, :if => :if_check_generate_device_gateway_aftersave_callback?

  # => Accessors
  attr_accessor :if_check_generate_device_gateway_aftersave_callback_var
  
  private

  def generate_device_gateway_url
    # => We generate device_gateway_url for this device_gateway based on its ID ...

    device_gateway_url = "#{self.base_url}/device_gateways/#{self.id}"
    #self.save(:validate => false)
    self.update_columns(device_gateway_url: device_gateway_url)
  end


end
