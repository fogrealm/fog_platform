class EndDeviceInfoInfraParameter < ApplicationRecord
  #include Formats::EndDeviceInfoInfraParameter

  # => Relations ...
  belongs_to :end_device_info
  belongs_to :infra_parameter


  # => Validations ..
  validates :end_device_info, :infra_parameter, presence: true

end
