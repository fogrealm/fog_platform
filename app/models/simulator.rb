class Simulator
  # => Constants ...
  SIMULATOR_DEFAULT_DATA_RETRIEVE_PERIOD = 4

  # => Accessors ...
  attr_accessor :current_service_infra_scenario, :current_data_retrieve_period, :current_device_gateway, :fog_api, :api_fetch_scheduler

  def initialize
    # => We initialize the connection instance with faraday framework ...
    # => We set the response type with application / json ...
    # => The form data is url encoded and we are using default net adapter using Net::HTTP ...

    self.fog_api = nil
    self.current_service_infra_scenario = ServiceInfra::SERVICE_INFRA_SCENARIO_NORMAL_ALL
    self.current_data_retrieve_period = SIMULATOR_DEFAULT_DATA_RETRIEVE_PERIOD
    self.api_fetch_scheduler = Rufus::Scheduler.new
  end

  
  def fetch_simulated_data(device_gateway)
    # => Function to perform API call to fetch simulated data from the server ...

    if self.fog_api.nil?
      self.fog_api = FogApi.new(device_gateway.base_url)
    end
      
    service_infra_scenario = self.current_service_infra_scenario || ServiceInfra::SERVICE_INFRA_SCENARIO_NORMAL_ALL
    data_retrieve_period = self.current_data_retrieve_period || SIMULATOR_DEFAULT_DATA_RETRIEVE_PERIOD
    device_gateway_id = device_gateway.id
    
    self.api_fetch_scheduler.every '2s' do

      simulator_mode = $redis.get("simulator_mode")
      if simulator_mode == "house_on_fire"
        service_infra_scenario = ServiceInfra::SERVICE_INFRA_SCENARIO_FIRE_OCCURS
        self.current_service_infra_scenario = ServiceInfra::SERVICE_INFRA_SCENARIO_FIRE_OCCURS
      else
        service_infra_scenario = ServiceInfra::SERVICE_INFRA_SCENARIO_NORMAL_ALL
        self.current_service_infra_scenario = ServiceInfra::SERVICE_INFRA_SCENARIO_NORMAL_ALL
      end

      self.update_cache_readings_from_using_device_gateway_id(device_gateway_id, service_infra_scenario, data_retrieve_period)
    end

    #self.api_fetch_scheduler.join
  end


  def update_cache_readings_from_using_device_gateway_id(device_gateway_id, service_infra_scenario, data_retrieve_period)
    api_response_body = nil
    response = self.fog_api.conn.get "/device_gateways/#{device_gateway_id}/generate_simulated_data.json", { :service_infra_scenario => service_infra_scenario, :data_retrieve_period => data_retrieve_period }
    Delayed::Worker.logger.info "\n\n response -> #{response.body} "
    api_response_body = response.body
    mapped_json_data = JSON.parse(api_response_body)
    cache_readings_from_mapped_json_data( mapped_json_data )
  end


  def cache_readings_from_mapped_json_data( mapped_json_data )
    simulated_data = mapped_json_data["simulated_data"]
    
    house_data = simulated_data["house"]
    latitude = house_data["location"]["latitude"].to_f
    longitude = house_data["location"]["longitude"].to_f

    gateway_timestamp = simulated_data["gateway_timestamp"].to_i

    end_devices_data = simulated_data["end_devices"]
    end_devices_data.each do |end_device_data|
      end_device_id = end_device_data["id"].to_i

      end_device_measurements_data = end_device_data["end_device_measurements"]
      end_device_measurements_data.each do |end_device_measurement_data|

        infra_parameter_name = end_device_measurement_data["name"]
        infra_parameter_unit = end_device_measurement_data["unit"]
        measurement = end_device_measurement_data["measurement"]

        value_to_be_cached = "#{end_device_id},#{infra_parameter_name},#{infra_parameter_unit},#{measurement},#{gateway_timestamp}"
        $redis.set("cached_end_device_#{end_device_id}_infra_parameter_#{infra_parameter_name}_unit_#{infra_parameter_unit}_reading", value_to_be_cached)
      end
    end

  end

  def stop_fetching_simulated_data
    self.api_fetch_scheduler.stop
  end

  class << self

    attr_accessor :simulator_mode
  
    def set_house_on_fire_mode
      $redis.set("simulator_mode", "house_on_fire")
    end

    def set_normal
      $redis.set("simulator_mode", "normal")
    end
  end
end