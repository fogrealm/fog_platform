class FogNetworkManager

  INTERCONNECTING_FOG_NODE_BASE_URL = "http://0.0.0.0"
  INTERCONNECTING_FOG_NODE_ROUTE_PATH = "/house_events/interconnecting_fog_controller.json"
  INTERCONNECTING_FOG_NODE_PORT = 3050

  CLOUD_TEST_EVENT_ROUTE_PATH = "/house_events/test_event_controller.json"
  CLOUD_SEND_INFRA_READING_PATH = "/infra_readings.json"
  CLOUD_SEND_HOUSE_EVENT_PATH = "/house_events.json"

  CLOUD_BASE_URL = "http://ec2-18-191-249-108.us-east-2.compute.amazonaws.com:3000"


  class << self

    def send_request_to_rails_controller(service_task_entity_map, rails_instance_port_no, controller_connections)
    
      network_operation_status = false
      
       
      max_retry_count = service_task_entity_map[:max_retry_count]
      retry_count = service_task_entity_map[:retry_count]

      while retry_count < max_retry_count

        opts = {}
        opts[:service_task_entity_map] = service_task_entity_map
        opts[:port_no] = rails_instance_port_no
        request_controller_spec = PlatformConfig.generate_request_controller_spec( opts )

        network_operation_status = true
    
        if request_controller_spec.present?

          route = request_controller_spec[:route]
          route_parameters = request_controller_spec[:route_parameters]

          puts "\n params -> #{route_parameters.to_json}"

            
          conn = controller_connections[rails_instance_port_no]
          response = conn.post route, route_parameters.to_json rescue network_operation_status = false
          
          if network_operation_status == false 
            priority = service_task_entity_map[:service_priority]
              
            retry_count = retry_count + 1
            service_task_entity_map[:retry_count] = retry_count + 1
            if rails_instance_port_no == ServiceTaskManager::RAILS_INSTANCES_PORT_RANGE.last
              rails_instance_port_no = RAILS_INSTANCES_PORT_RANGE.first
            else 
              rails_instance_port_no = rails_instance_port_no + 1
            end

            #service_task_pqueue.push service_task_entity_map, service_task_entity_map[:enqueued_timestamp]
          end
        else
          retry_count = retry_count + 1
          service_task_entity_map[:retry_count] = retry_count + 1
        end  
        
        return if network_operation_status == true
    
      end
      
      if network_operation_status == false
        ServiceTask.save_in_db!(service_task_entity_map)
        # => Write code to inform cloud as well ...

      end
    
    end

    def send_meter_reading_to_interconnecting_fog_node(house_id, infra_parameter_id, measurement)
      

      fog_api = FogApi.new("#{INTERCONNECTING_FOG_NODE_BASE_URL}:#{INTERCONNECTING_FOG_NODE_PORT}")
        
      payload = {
        house_id: house_id,
        infra_parameter_id: infra_parameter_id,
        measurement: measurement
      }

      response = fog_api.conn.post INTERCONNECTING_FOG_NODE_ROUTE_PATH, payload.to_json


      
      fog_api = FogApi.new(CLOUD_BASE_URL)
        
      payload = {
        kwh: measurement,
      }

      response = fog_api.conn.post CLOUD_TEST_EVENT_ROUTE_PATH, payload.to_json
      

    end


    def send_meter_reading_info_to_cloud(house_id, infra_parameter_id, measurement)
      fog_api = FogApi.new(CLOUD_BASE_URL)
        
      payload_data = {
        value: measurement,
        infra_parameter_id: infra_parameter_id,
        end_device_id: 1
      }

      payload = {}
      payload[:infra_reading] = payload_data

      response = fog_api.conn.post CLOUD_SEND_INFRA_READING_PATH, payload.to_json

    end


    def send_fire_detection_info_to_cloud(name)
      fog_api = FogApi.new(CLOUD_BASE_URL)
        
      payload_data = {
        name: name,
        house_event_type: HouseEvent::HOUSE_EVENT_TYPE_ALERT,
        event_level: 9,
        end_device_id: 1
      }

      payload = {}
      payload[:house_event] = payload_data

      response = fog_api.conn.post CLOUD_SEND_HOUSE_EVENT_PATH, payload.to_json

    end


    handle_asynchronously :send_request_to_rails_controller
    handle_asynchronously :send_meter_reading_to_interconnecting_fog_node
    handle_asynchronously :send_meter_reading_info_to_cloud
    handle_asynchronously :send_fire_detection_info_to_cloud
  end

end