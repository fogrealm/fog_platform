module Actions::HouseEvent

  def self.included(receiver)
    receiver.extend         ClassMethods
    receiver.send :include, InstanceMethods
  end


  module ClassMethods
    
    def monitor_fire_occurence

      return if HouseEvent.fire_detection_scheduler.present?

      HouseEvent.fire_detection_scheduler = Rufus::Scheduler.new

      house_id = House.first.id
      house_section_ids = ServiceInfra.first.house_section_ids
      infra_parameter_ids = ServiceInfra.first.infra_parameter_ids
      all_admin_users = AdminUser.all

      measurement_map = {}
      fire_detection_warning_count = 2
      fire_alarm_max_sms_allowed = 3

      fire_detection_scheduler.every '4s' do
        
        house_section_ids.each do |house_section_id|
          infra_parameter_ids.each do |infra_parameter_id|
            measurement_map[infra_parameter_id] = $redis.get("controller_cached_house_section_id_#{house_section_id}_infra_parameter_id_#{infra_parameter_id}_reading")
            measurement_map[infra_parameter_id] = measurement_map[infra_parameter_id].to_f if measurement_map[infra_parameter_id].present?
          end

          # => Testing Smoldering Fire ...
          temperature = measurement_map[1]
          co2 = measurement_map[3]



          voltage_gs822 = measurement_map[4]
          voltage_tgs880 = measurement_map[5]

          co = measurement_map[2]
          smoke_voltage = measurement_map[6]

          ion_voltage = measurement_map[7]

          fire_type = :nuisance



          $house_events_logger.info "\n\n house_section - #{house_section_id}"
          $house_events_logger.info " temperature - #{temperature}" if temperature.present?        
          $house_events_logger.info " co - #{co}" if co.present?
          $house_events_logger.info " co2 - #{co2}" if co2.present?
          $house_events_logger.info " voltage_gs822 - #{voltage_gs822}" if voltage_gs822.present?
          $house_events_logger.info " voltage_tgs880 - #{voltage_tgs880}" if voltage_tgs880.present?
          $house_events_logger.info " smoke_voltage - #{smoke_voltage}" if smoke_voltage.present?
          $house_events_logger.info " ion_voltage - #{ion_voltage}" if ion_voltage.present?



          if temperature.present? && co2.present?
            if co2 > 210.00 && temperature > 40.556
              fire_type = :flaming
            end
          elsif voltage_gs822.present? && voltage_tgs880.present?
            if voltage_gs822 > 0.9 && voltage_tgs880 > 0.15
              fire_type = :nuisance
            end
          elsif co.present? && co2.present? && voltage_gs822.present?
            if co > 17.00 && co2 > 22.00 && voltage_gs822 > 0.27
              fire_type = :smoldering
            end
          end

          if fire_type != :nuisance
            fire_alarm_detection_count = $redis.get("fire_alarm_detection_count").to_i
            #fire_alarm_max_sms_send =  $redis.get("fire_alarm_max_sms_send").to_i

            $house_events_logger.info "Fire Warning"
            if fire_alarm_detection_count > fire_detection_warning_count# && $redis.get("fire_detection_occurence_time").to_i == nil
              
              # => Send Event ...
              house_section_name = HouseSection.find(house_section_id).name
              $house_events_logger.info " Fire Alarm System - Detected Fire Scenario at #{Time.now.to_s} - #{house_section_name}"
              $house_events_logger.info " Informing Users - #{all_admin_users.as_json }} "

              # => Send SMS here ...
              phone_numbers = all_admin_users.map(&:phone_number)
              
              send_sms_status = $redis.get("send_sms_status")
              informed_cloud = $redis.get("inform_cloud")

              if send_sms_status.nil?
                smsjob = SmsManager.send_sms(to: phone_numbers, message: "Important! Fire Detected (#{fire_type}) - #{house_section_name}, time - #{Time.now.to_s}")
                smsjob.invoke_job rescue nil
                $redis.setex("send_sms_status",10 * 60, "send")
              end

              if informed_cloud != "informed"
                FogNetworkManager.send_fire_detection_info_to_cloud("Important! Fire Detected (#{fire_type}) - #{house_section_name}, time - #{Time.now.to_s} ")
                $redis.set("inform_cloud","informed")
              end

              if $redis.get("fire_detection_occurence_time") == nil
                $redis.setex("fire_detection_occurence_time", 60 * 60, Time.now.to_i)
              end
            else 
              fire_alarm_detection_count = fire_alarm_detection_count + 1
              $redis.set("fire_alarm_detection_count", fire_alarm_detection_count)
            end
          else
            $house_events_logger.info " Nuisance. Don't worry"
          end
        end 

        
      end



    end


    def monitor_smart_meter

      return if HouseEvent.smart_meter_scheduler.present?

      house_id = House.first.id
      house_section_ids = ServiceInfra.second.house_section_ids
      infra_parameter_ids = ServiceInfra.second.infra_parameter_ids

      HouseEvent.smart_meter_scheduler = Rufus::Scheduler.new


      HouseEvent.smart_meter_scheduler.every '10s' do
        # => Send to interconnecting fog node ...

        house_section_ids.each do |house_section_id|  
          infra_parameter_ids.each do |infra_parameter_id|
            measurement = $redis.get("controller_cached_house_section_id_#{house_section_id}_infra_parameter_id_#{infra_parameter_id}_reading")
            FogNetworkManager.send_meter_reading_to_interconnecting_fog_node(house_id, infra_parameter_id ,measurement)
          end
        end
        

        end

    end


    def enable_interconnecting_forward_scheduler
      return if HouseEvent.icfn_forwarding_scheduler.present?

      
      HouseEvent.icfn_forwarding_scheduler = Rufus::Scheduler.new

      HouseEvent.icfn_forwarding_scheduler.every '8m' do
        # => Need to be changed the timer into one hour ...

        house_ids_json = $redis.get("cached_house_ids_json")

        if house_ids_json.present? && house_ids_json != ""
          house_ids = JSON.parse(house_ids_json)

          house_ids.each do |house_id|
            # => smart meter reading - infraparameter Electric Consumption kwh reading ...hardcoding now ...
            infra_parameter_id = 8

            measurement = $redis.get("cached_icfn_entry_#{house_id}_#{infra_parameter_id}")
            FogNetworkManager.send_meter_reading_info_to_cloud(house_id, infra_parameter_id, measurement)
          end
        end
      end


    end
    

  end
  
  module InstanceMethods

    
  end
=begin  

  {2=>["CO", "ppm"], 
    3=>["CO2", "ppm"], 
    4=>["MOX GS822", "V"], 
    1=>["Temperature", "celcius"], 
    5=>["MOX TGS880", "V"], 
    6=>["Smoke Voltage", "V"], 
    7=>["Ion Voltage", "V"], 
    8=>["Electric Consumption", "kwh"]}
=end

end
=begin
    ire_type = nuisance
If CO2 > 210 ppm or T > 40.556 celcius then
fire_type = Flaming fire.
ElseIf VT(GS822) > 0.9 V and V(TGS880) > 0.15 V then
fire_type = Nuisance.
Elseif CO > 17 ppm and CO2 > 22 ppm and V T(GS822) > 0.27 V then
fire_type = Smoldering fire.
EndIf
=end


=begin
  
  service_task_entity_map = {
      :gateway_timestamp => gateway_timestamp,
      :service_priority => priority,
      service_infra: {
        id: service_infra.id,
        name: service_infra.name,
        priority: service_infra.priority
      },
      :infra_reading => {
        :infra_parameter => {
          :id => infra_parameter_id,
          :name => infra_parameter_name,
          :unit => infra_parameter_unit
        },
        :value => measurement.to_f,
        :end_device => {
          :id => end_device_id
        }
      },
      :retry_count => 0,
      :max_retry_count => ServiceTask::END_DEVICE_DATA_INFORM_MAX_RETRY_COUNT,
      :enqueued_timestamp => Time.now.to_i,
      :service_task_type => ServiceTask::SERVICE_TASK_TYPE_END_DEVICE_DATA_INFORMATION
    }


=end