class EndDevice < ApplicationRecord
  # include Formats::EndDevice
  #include Conditions::EndDevice

  # => Constants ...
=begin
  END_DEVICE_CLASS_DEVICE = "device"
  END_DEVICE_CLASS_GATEWAY = "gateway"

  END_DEVICE_CLASSES = [
    END_DEVICE_CLASS_DEVICE,
    END_DEVICE_CLASS_GATEWAY
  ]
=end

  # => Relations ...
  has_many :house_events
  has_many :infra_parameters, :through => :end_device_info 
  has_many :infra_readings
  #has_many :end_devices

  belongs_to :end_device_info
  belongs_to :house_section
  belongs_to :service_infra
  belongs_to :device_gateway


  # => Validations ...
  validates :name, presence: true
  validates :end_device_info, :house_section, :device_gateway, presence: true
  validates :should_alert_status, inclusion: { :in => [ true, false ] }
  validates :serial_number, presence: true
  
  validates_uniqueness_of :serial_number

  # => Delegations ...
  delegate :house, to: :house_section

  # useful for fast creation of end devices in admin panel ...
  # before_validation do
  #  self.name = "#{self.end_device_info.name} Device  #{self.house_section.name}"
  # end

  def tester_dragon
    Delayed::Worker.logger.info("\n\n Dragon making Entry on Delayed Logger")
  end
  
  #validate :ensure_valid_gateway_assigned

=begin
  # => Callbacks ...
  before_validation on: :create do
    # => If no end device class mentioned, we take the end device class as 'device'

    if self.end_device_class.nil?
      self.end_device_class = END_DEVICE_CLASS_DEVICE
    end
  end


  private

  def ensure_valid_gateway_assigned
    # => Ensure valid gateway is assigned to the current End Device ...
    
    device_gateway = self.device_gateway

    if self.is_device_class_device?
      # => If the current Device class is a device. We check whether it has gateway assigned ...
      # => If assigned a gateway, we check whether that is a valid gateway ...

      if device_gateway.nil?
        errors[:End_Device] << " must be assigned with a gateway "
        return
      end

      if device_gateway.is_device_class_device?
        errors[:End_Device] << " has a gateway assigned which is actually an End Device "
        return
      end
    
    elsif self.is_device_class_gateway?
      # => If the current Device class is a gateway. You cannot further assign a gateway to it ...
      
      if device_gateway.present?
        errors[:End_Device] << " cannot be assigned with a gateway since it is itself a Gateway "
        return
      end 
    end
  end

=end

end