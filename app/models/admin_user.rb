class AdminUser < ApplicationRecord
  include Formats::AdminUser

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable


  # => Validations ...
  validates :name, presence: true
  validates :phone_number, presence: true, format: { with: ActiveRecordBaseCommon::Validations::VALID_PHONE_FORMAT }
  
  validates_uniqueness_of :phone_number


end
