class AddColumnsToServiceTasks < ActiveRecord::Migration[5.0]
  def change
    add_reference :service_tasks, :service_infra, index: true
    add_reference :service_tasks, :infra_reading, index: true

    add_column :service_tasks, :service_priority, :integer, index: true, default: ServiceInfra::MIN_PRIORITY
    add_column :service_tasks, :gateway_timestamp, :integer

    add_column :service_tasks, :retry_count, :integer, index: true
    add_column :service_tasks, :max_retry_count, :integer, index: true
    add_column :service_tasks, :is_drop_allowed, :boolean, default: false
    add_column :service_tasks, :service_task_type, :string, default: ServiceTask::SERVICE_TASK_TYPE_OTHERS, index: true
  end
end
