class CreateDeviceGateways < ActiveRecord::Migration[5.0]
  def change
    create_table :device_gateways do |t|
      
      t.string :name
      t.string :device_gateway_url

      t.timestamps
    end
  end
end
