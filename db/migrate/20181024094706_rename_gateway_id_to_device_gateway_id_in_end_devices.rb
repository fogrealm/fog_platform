class RenameGatewayIdToDeviceGatewayIdInEndDevices < ActiveRecord::Migration[5.0]
  def up
    remove_column :end_devices, :gateway_id, :string
    add_column :end_devices, :device_gateway_id, :integer, index: true
  end

  def down
    remove_column :end_devices, :device_gateway_id, :integer
    add_column :end_devices, :gateway_id, :string, index: true
  end
end
