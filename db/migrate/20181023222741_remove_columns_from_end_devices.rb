class RemoveColumnsFromEndDevices < ActiveRecord::Migration[5.0]
  def change
    remove_column :end_devices, :end_device_class, :string
    remove_column :end_devices, :device_url, :string
  end
end
