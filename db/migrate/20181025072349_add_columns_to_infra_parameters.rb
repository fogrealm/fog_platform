class AddColumnsToInfraParameters < ActiveRecord::Migration[5.0]
  def change
    add_column :infra_parameters, :min_value, :integer
    add_column :infra_parameters, :max_value, :integer
    add_column :infra_parameters, :fractional_accuracy, :integer
  end
end
