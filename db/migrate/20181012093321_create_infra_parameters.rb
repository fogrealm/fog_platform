class CreateInfraParameters < ActiveRecord::Migration[5.0]
  def change
    create_table :infra_parameters do |t|

      t.string :name, unique: true
      t.string :unit

      t.timestamps
    end
  end
end