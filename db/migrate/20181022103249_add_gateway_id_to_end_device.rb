class AddGatewayIdToEndDevice < ActiveRecord::Migration[5.0]
  def change
    add_column :end_devices, :gateway_id, :string, index: true
  end
end
