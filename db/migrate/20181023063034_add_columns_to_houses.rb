class AddColumnsToHouses < ActiveRecord::Migration[5.0]
  def change
    add_column :houses, :address, :text
    add_column :houses, :landmark, :string
    add_column :houses, :description, :text
  end
end
