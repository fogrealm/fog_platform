class AddDeviceUrlToEndDevices < ActiveRecord::Migration[5.0]
  def change
    add_column :end_devices, :device_url, :string, unique: true
  end
end
