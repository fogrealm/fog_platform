class AddValueToInfraReadings < ActiveRecord::Migration[5.0]
  def change
    add_column :infra_readings, :value, :float
  end
end
