class AddPriorityToServiceInfra < ActiveRecord::Migration[5.0]
  def change
    add_column :service_infras, :priority, :integer, index: true
  end
end
