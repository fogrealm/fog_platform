class AddUniqueIndexToNameUnitInInfraParameters < ActiveRecord::Migration[5.0]
  def change
    add_index :infra_parameters, [ :name, :unit], unique: true
  end
end
