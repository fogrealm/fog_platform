class CreateEndDeviceInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :end_device_infos do |t|
      t.string :name
      t.string :description
      t.string :model
      t.string :manufacturer

      t.timestamps
    end
  end
end
