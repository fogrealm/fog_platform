class AddColumnsToInfraReadings < ActiveRecord::Migration[5.0]
  def change
    add_reference :infra_readings, :infra_parameter, index: true
    add_reference :infra_readings, :end_device, index: true
  end
end
